package Loso;
import java.util.ArrayList;
/**
 * Game is a Abstract class 
 * 1. provide the constructor for initial variables
 *    and getter/setter for accessing variables
 * 2. Having a protected static variable for counting
 *    the game round number  
 * 3. Three abstract methods:
 *  - public int generateTime() 
 *  subclasses implement its own time limitation
 * @return int - random a number between its own limitation
 *  - public boolean pickCandidate(int candidateLimit)
 * @param int candidateLimit
 * @return boolean : to verify the behavior is complete or not
 *  subclasses implement its own candidate list
 *  - public boolean executeGame()   <--------should be extract common section
 *  subclasses implement its own game
 * @return boolean : to verify the behavior is complete or not
 */
public abstract class Game {
	public static final int GAME_SWIMMING   = 1;
	public static final int GAME_CYCLING    = 2;
	public static final int GAME_RUNNING    = 3;
	public static final int GAMETYPE_NUMBER = 3; //total 3 types of game
	public static final int CANDIDATELIMIT_MIN = 4;
	public static final int CANDIDATELIMIT_MAX = 8;
	
	public static int gameRoundNum = 0;
	
	private String gameType;
	private String gameID;
	
	private Participant        referee;
	private String             gameResult;
	private ArrayList<Athlete> candidate;
	private Athlete            predictWinner;
	
	public Game(String gameType)
	{
		this.gameRoundNum++;
		setGameID(gameType);
	}
	
	public void setGameID(String gameType)
	{
		this.gameType = gameType;
		this.gameID = gameType + String.format("%02d", gameRoundNum);
	}
	public String getGameID()
	{
		return this.gameID;
	}
	public ArrayList<Athlete> getCandidate() {
		return this.candidate;
	}
	public void addCandidate(Athlete candidate) {
		if(this.candidate == null)
			this.candidate = new ArrayList<Athlete>();
		this.candidate.add(candidate);
	}
	public Participant getReferee() {
		return referee;
	}
	public void setReferee(Participant referee) {
		this.referee = referee;
	}
	public String getGameResult() {
		return gameResult;
	}
	public void setGameResult(String gameResult) {
		this.gameResult = gameResult;
	}
	public Athlete getPredictWinner() {
		return predictWinner;
	}
	public void setPredictWinner(Athlete predictWinner) {
		this.predictWinner = predictWinner;
	}
	
	//abstract public 
	abstract public int generateTime();
	abstract public boolean pickCandidate(int candidateLimit);
	abstract public boolean executeGame();
}
