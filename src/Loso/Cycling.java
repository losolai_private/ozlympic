package Loso;
import java.util.*;

/**
 * inheritance from Game
 * initial the basic information through superclass constructor
 * override the three method
 * 1. int generateTime()
 * 2. boolean pickCandidate(int candidateLimit)
 * 3. boolean executeGame()
 */
public class Cycling extends Game {
	private final int TIMELIMITE_MIN   = 500;
	private final int TIMELIMITE_MAX   = 800;
	private static String GAMETYPE_CYCLE = "C";

	public Cycling()
	{
		super(GAMETYPE_CYCLE);
		
		//setting official and candidate info.
		boolean bValidate = pickCandidate(Game.CANDIDATELIMIT_MAX);
		if(!bValidate)
			OzlympicGame.errorMsg_InvalidateCandidateList();
	}

	@Override
	public int generateTime()
	{
		Random rand = new Random();
		int randomNum = rand.nextInt((TIMELIMITE_MAX - TIMELIMITE_MIN) + 1) + TIMELIMITE_MIN;
		return randomNum;
	}
	
	@Override
	public boolean pickCandidate(int candidateLimit)
	{
		ArrayList<Participant> cyclistList = Driver.participantList.get(Participant.CYCLIST);
		ArrayList<Participant> officalList = Driver.participantList.get(Participant.OFFICAL);
		//randomize list data
		Collections.shuffle(cyclistList);
		Collections.shuffle(officalList);
		
		Participant person;
		
		int candidateNum = candidateLimit;
		if(cyclistList.size() < candidateNum)
			candidateNum = cyclistList.size();
		//checking candidate limitation
		if(candidateNum < Game.CANDIDATELIMIT_MIN)
			return false;
			
		for(int i=0 ; i<candidateNum ; i++)
		{
			person = cyclistList.get(i);
			if(person == null)
				continue;
			if(person instanceof Cyclist)
				super.addCandidate((Cyclist)person);
			if(person instanceof SuperAthlete)
				super.addCandidate((SuperAthlete)person);
		}
		
		//always set index 0 as a game referee
		person = officalList.get(0);
		if(person instanceof Offical && person != null)
			super.setReferee(person);
		return true;
	}
	
	@Override
	public boolean executeGame()
	{
		if(super.getCandidate() == null || super.getReferee() == null)
			return false;
		
		ArrayList<Athlete> athList = super.getCandidate();
		String str = "";
		//execute athlete complete()
		for(int i=0 ; i<athList.size() ; i++)
		{
			Athlete candidate = athList.get(i);
			if(candidate == null)
				continue;
			
			//use setExecuteTime to storeData
			int completeSec = candidate.Complete();
			candidate.setExecuteTime(completeSec);
			str += candidate.toString();
		}
		// showing result here <need to store in referee>
		System.out.println(this.getGameID() + " result:\n" +
						   "-------------------------------\n" + str);
		//after sorting
		Collections.sort(athList);
		//setting info. into referee
		Offical referee;
		if(super.getReferee() instanceof Offical)
		{
			referee = ((Offical)super.getReferee());
			setGameResult(referee.setResultTopList(this.getGameID(), athList));
			//display game result
			System.out.println(getGameResult() + "\n");
			//reset referee's game result
			referee.resetGameResult();
		}
						
		// For Test _ display winner & predict info.
		System.out.println("The Winner: " + athList.get(0).toString() + "\n" +
						   "Your Predict: " + super.getPredictWinner().toString() +
						   "\n-------------------------------\n");
		//checking predict winner
		if(super.getPredictWinner() == athList.get(0))
			System.out.println("Congratulation: The predict athlete won the game!!");
		
		return true;
	}
}
