package Loso;
import java.util.ArrayList;
/**
 * Offical inheritance Participant class 
 * 1. with two extra variables for recording 
 *    the ranking list which contain Athlete info. and game result
 * 2. provide setResultTopList() method
 * - generate game result by giving two params
 * @param : String gameID, ArrayList<Athlete> sortedList
 * @return String : game result 
 */
public class Offical extends Participant {
	public static final int RESULT_TOP3 = 3;
	private final int INDEX_1ST = 0;
	private final int INDEX_2ND = 1;
	private final int INDEX_3RD = 2;
	private final double POINT_1ST = 5.0;
	private final double POINT_2ND = 2.0;
	private final double POINT_3RD = 1.0;
	
	private Athlete[] resultTop3;
	private String gameResult;
	
	public Offical(String name, int age, String state)
	{
		super(name, age, state, OFFICAL);
		this.resultTop3 = new Athlete[RESULT_TOP3];
		this.gameResult = "";
	}

	public String getGameResult() {
		return gameResult;
	}
	public void resetGameResult()
	{
		this.gameResult = "";
	}

	public Athlete[] getResultTopList() {
		return resultTop3;
	}
	public String setResultTopList(String gameID, ArrayList<Athlete> sortedList) 
	{
		//display gameID first
		this.gameResult = "========= " + gameID + " result ==========";
		if(sortedList != null)
		{
			int index = 0;
			while(index < RESULT_TOP3)
			{
				Athlete record = sortedList.get(index);
				if(index == INDEX_1ST)
					record.setPoints(POINT_1ST);
				if(index == INDEX_2ND)
					record.setPoints(POINT_2ND);
				if(index == INDEX_3RD)
					record.setPoints(POINT_3RD);
					
				resultTop3[index] = record;
				this.gameResult += record.toString() + "\t" + record.getPoints();
				index++;
			}
			this.gameResult += "\n";
		}
		return this.gameResult;
	}
}
