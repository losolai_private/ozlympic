package Loso;
/**
 * inheritance from Athlete
 * initial the basic information through superclass constructor
 */
public class Sprinter extends Athlete {
	
	public Sprinter(String name, int age, String state)
	{
		super(name, age, state, SPRINTER);
	}
}
