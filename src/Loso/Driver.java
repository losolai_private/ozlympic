package Loso;

import java.util.*;

/* The role of Driver is game controller
 * which means it control all the logic to execute each game round
 * Functions: processByUserInput()
 * - implement the specific methods by user input
 * 1. initialParticipantList()
 * 2. createGameByInput()
 * 3. displayCandidateList()
 * 4. displayAllGameResult()
 * 5. displayAllAthletePoints()
 * Addd by LosoLai 19/03/17
 */

public class Driver {
	public static HashMap<String, ArrayList<Participant>> participantList = 
												new HashMap<String, ArrayList<Participant>>();
	
	public static Game currentGame;
	private ArrayList<Game> gameList = new ArrayList<Game>();	
	private ArrayList<Participant> swimmerList = new ArrayList<Participant>();
	private ArrayList<Participant> cyclistList = new ArrayList<Participant>();
	private ArrayList<Participant> sprinterList = new ArrayList<Participant>();
	private ArrayList<Participant> superAthList = new ArrayList<Participant>();
	private ArrayList<Participant> officalList = new ArrayList<Participant>();
	
	//private Athlete predictWinner;
	
	public Driver()
	{
		boolean bProcessResult = initialParticipantList();
		if(!bProcessResult)    //initial data fail
			System.out.println("Read file fail!!");
		else 
			System.out.println("Initial participant list successfully!!");
	}
	
	public boolean processByUserInput(int userInput)
	{
		if(userInput != OzlympicGame.SELECT_GAME)
		{
			//check game status
			if(OzlympicGame.gameStatus == OzlympicGame.GAME_DEFAULT ||
					   currentGame == null)
				return OzlympicGame.errorMsg_GameUninitialized();	
		}
		
		//implement game control
		switch(userInput)
		{
			case OzlympicGame.SELECT_GAME:
			{
				//check game status to prevent initialize without executing
				if(OzlympicGame.gameStatus == OzlympicGame.GAME_INITIALED)
					return OzlympicGame.errorMsg_GameUnexecuted();
				
				Scanner reader = new Scanner(System.in);
				int gameType = 0;
				
				OzlympicGame.displayGameTypeMenu();
				String sInput = reader.next();
				try
				{
					//get game type
					gameType = Integer.parseInt(sInput);
				}
				catch(Exception e){
					OzlympicGame.showUserChoiceWarning(sInput);
				}
				
				boolean bValidate = OzlympicGame.inputValidation_Sub(gameType);
				if(!bValidate)
					break;
				
				// create a Game obj by userInput
				createGameByInput(gameType);
				
				OzlympicGame.gameStatus = OzlympicGame.GAME_INITIALED;
			}
				break;
			case OzlympicGame.PREDIT_WINNER:
			{
				boolean bFuncWork = displayCandidateList();
				if(!bFuncWork)
					return true; //back to main menu
			}
				break;
			case OzlympicGame.START_GAME:
			{
				//re-run game checking
				if(OzlympicGame.gameStatus == OzlympicGame.GAME_EXECUTED)
					currentGame.setPredictWinner(null); //clear predict choice
				
				boolean bExecuted = currentGame.executeGame();
				if(bExecuted)
					OzlympicGame.gameStatus = OzlympicGame.GAME_EXECUTED;
			}
				break;
			case OzlympicGame.DISPLAY_FINALRESULT:
			{
				//display game result
				displayAllGameResult();
			}
				break;
			case OzlympicGame.DISPLAY_ATHLETEPOINTS:
			{
				displayAllAthletePoints();
			}
				break;
			case OzlympicGame.EXIT_GAME:
			default:
				return false;
		}
		return true;
	}
	
	private boolean initialParticipantList()
	{
		//setting dummy data here for testing
		//need to use read file to set info.
		
		for(int i=0 ; i<6 ; i++)
		{
			//setting athlete
			//name format using (personType + id)
			String name = "ATH-" + Swimmer.SWIMMER 
					             + Integer.toString(i);
			int age = 20 + i;
			Participant swimmer = new Swimmer(name, age, "VIC");
			swimmerList.add(swimmer);
			
			
			name = "ATH-" + Cyclist.CYCLIST
		                  + Integer.toString(i);
			Participant cyclist = new Cyclist(name, age, "VIC");
			cyclistList.add(cyclist);
			
			name = "ATH-" + Sprinter.SPRINTER
	                      + Integer.toString(i);
			Participant sprinter = new Sprinter(name, age, "VIC");
			sprinterList.add(sprinter);
			
			name = "ATH-" + SuperAthlete.SUPERATHLETE 
                          + Integer.toString(i);
			Participant superAthlete = new SuperAthlete(name, age, "VIC");
			superAthList.add(superAthlete);
			
			//for implement candidate list
			//adding superAth into each list
			swimmerList.add(superAthlete);
			cyclistList.add(superAthlete);
			sprinterList.add(superAthlete);
			
			//setting offical
			name = "OFF-" + Integer.toString(i);
			Participant offical = new Offical(name, age, "VIC");
			officalList.add(offical);
		}
		
		participantList.put(Participant.SWIMMER, swimmerList);
		participantList.put(Participant.CYCLIST, cyclistList);
		participantList.put(Participant.SPRINTER, sprinterList);
		participantList.put(Participant.OFFICAL, officalList);

		//test 
		//System.out.println(swimmerList);
		//System.out.println(cyclistList);
		//System.out.println(sprinterList);
		
		return true;
	}
	
	private boolean createGameByInput(int gameType)
	{
		if(gameType == Game.GAME_RUNNING)
			currentGame = new Running();
		else if(gameType == Game.GAME_SWIMMING)
			currentGame = new Swimming();
		else
			currentGame = new Cycling();
		gameList.add(currentGame);
		
		//test - display gameID 
		System.out.println(currentGame.getGameID());
		return true;
	}
	private boolean displayCandidateList()
	{
		ArrayList<Athlete> candList = currentGame.getCandidate();
		if(candList != null)
		{
			System.out.print("Candidate List\n" +
					 		 "===================================\n");
			String candListMenu = "";
			for(int i=0 ; i<candList.size() ; i++)
			{
				Participant candInfo = candList.get(i);
				if(candInfo != null)//+ candInfo.getName() + "\n"; showing name probably is better
					candListMenu += Integer.toString(i+1) + ". " 
									+ candInfo.getPersonID() + "\n"; 
			}
			System.out.print(candListMenu + "\nEnter an option:" );
			
			Scanner reader = new Scanner(System.in);
			String sInput = reader.next();
			int perdict = 0;
			try
			{
				perdict = Integer.parseInt(sInput);
			}
			catch(Exception e){
				OzlympicGame.showUserChoiceWarning(sInput);
			}
			
			boolean bValidate = OzlympicGame.inputValidation_Sub(perdict, candList.size());
			if(!bValidate)
				return false;
			
			//save the predict info.
			currentGame.setPredictWinner(candList.get(perdict-1));
		}
		return true;
	}
	private void displayAllGameResult()
	{
		String result = "";
		for(int i=0 ; i<gameList.size() ; i++)
		{
			Game gameInfo = gameList.get(i);
			if(gameInfo != null)
				result += gameInfo.getGameResult();
		}
		System.out.println(result);
	}
	private void displayAllAthletePoints()
	{
		//need to remove all superAthlete from each list first
		swimmerList.removeAll(superAthList);
		String swinnersResult = "===== Swimmers result =====\n";
		for(int i=0 ; i<swimmerList.size() ; i++)
		{
			Participant swimmer = swimmerList.get(i);
			if(swimmer != null && swimmer instanceof Swimmer)
			{
				double point = ((Swimmer)swimmer).getPoints();
				swinnersResult += (swimmer.getPersonID() + 
								   "\t" + Double.toString(point) + "\n");
			}
		}
		System.out.println(swinnersResult);
		
		String cyclistsResult = "===== Cyclists result =====\n";
		cyclistList.removeAll(superAthList);
		for(int i=0 ; i<cyclistList.size() ; i++)
		{
			Participant cyclist = cyclistList.get(i);
			if(cyclist != null && cyclist instanceof Cyclist)
			{
				double point = ((Cyclist)cyclist).getPoints();
				cyclistsResult += (cyclist.getPersonID() + 
								   "\t" + Double.toString(point) + "\n");
			}
		}
		System.out.println(cyclistsResult);
		
		String sprintersResult = "===== Sprinters result =====\n";
		sprinterList.removeAll(superAthList);
		for(int i=0 ; i<sprinterList.size() ; i++)
		{
			Participant sprinter = sprinterList.get(i);
			if(sprinter != null && sprinter instanceof Sprinter)
			{
				double point = ((Sprinter)sprinter).getPoints();
				sprintersResult += (sprinter.getPersonID() + 
									"\t" + Double.toString(point) + "\n");
			}
		}
		System.out.println(sprintersResult);
		
		String superAthResult = "===== SuperAthlete result =====\n";
		for(int i=0 ; i<superAthList.size() ; i++)
		{
			Participant superAth = superAthList.get(i);
			if(superAth != null && superAth instanceof SuperAthlete)
			{
				double point = ((SuperAthlete)superAth).getPoints();
				superAthResult += (superAth.getPersonID() + 
								   "\t" + Double.toString(point) + "\n");
			}
		}
		System.out.println(superAthResult);
	}
}
