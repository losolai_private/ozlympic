package Loso;
/**
 * Athlete inheritance Participant class 
 * 1. with two extra variables for recording 
 *    complete time and points
 * 2. multi-inheritance interface : Completeable & Comparable
 * @Override Complete()
 * - get the complete time by current game type
 * @return int : the time that athlete complete the game 
 * @Override compareTo(Athlete comparePerson)
 * - for sorting purpose
 * @return int : compare executeTime within two athlete objects
 */
public class Athlete extends Participant implements Completeable, Comparable<Athlete>{
	private int    executeTime;
	private double points;
	
	public Athlete(String name, int age, String state, String athleteType)
	{
		super(name, age, state, athleteType);
		this.setExecuteTime(0);
		this.setPoints(0.0);
	}

	public int getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(int executeTime) {
		this.executeTime = executeTime;
	}
	public double getPoints() {
		return points;
	}

	public void setPoints(double points) {
		this.points += points;
	}
	
	@Override
	public int Complete()
	{
		int completeSec = Driver.currentGame.generateTime(); 
		return completeSec;
	}
	
	@Override
	public int compareTo(Athlete comparePerson)
	{
		int compareTime = comparePerson.getExecuteTime();
		return this.executeTime - compareTime;
	}
	
	@Override
    public String toString() 
	{
        return String.format("\n" + super.getPersonID() + 
        					 ":\t\t" + this.getExecuteTime());
    }
}
