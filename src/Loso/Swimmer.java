package Loso;
/**
 * inheritance from Athlete
 * initial the basic information through superclass constructor
 */
public class Swimmer extends Athlete {
	
	public Swimmer(String name, int age, String state)
	{
		super(name, age, state, SWIMMER);
	}
}
