package Loso;
import java.util.Scanner;

/* Functions:
 * 1. Display main menus for getting user input
 * 2. OzlympicGame create a Driver object
 *    for implement the game controller part.
 * 3. Provide static method for showing error/validation messages   
 * Addd by LosoLai 19/03/17
 */
public class OzlympicGame {
	//define main menu option
	public static final int SELECT_GAME           = 1;
	public static final int PREDIT_WINNER         = 2;
	public static final int START_GAME            = 3;
	public static final int DISPLAY_FINALRESULT   = 4;
	public static final int DISPLAY_ATHLETEPOINTS = 5;
	public static final int EXIT_GAME             = 6;
	
	//define game status
	public static final int GAME_DEFAULT   = 0;
	public static final int GAME_INITIALED = 1;
	public static final int GAME_EXECUTED  = 2;
	public static int gameStatus = GAME_DEFAULT;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		Driver gameDriver = new Driver();
		
		int userInput = 0;
		String sInput = "";
		
		boolean bException = false;
		boolean bValidate = false;
		boolean bEndGame = false;
		
		do{
			//display the main menu
			OzlympicGame.displayMenu();
			sInput = reader.next();
			try
			{
				//get user input
				userInput = Integer.parseInt(sInput);
			}
			catch(Exception e){
				bException = showUserChoiceWarning(sInput);
			}
			
			if(bException)
				continue;
			
			bValidate = inputValidation_Main(userInput);
			if(!bValidate)
				continue;
			
			if((userInput == EXIT_GAME))
				break;
			
			bEndGame = !(gameDriver.processByUserInput(userInput));
		}
		while((userInput != EXIT_GAME) && (!bEndGame));
		
		OzlympicGame.gameOver();
	}

	//display menu, error messages & validation checking----------------------------
	
	public static void displayMenu()
	{
		System.out.print("Ozlympic Game\n" +
						 "===================================\n" + 
						 "1. Select a game to run\n" +
						 "2. Predict the winner of the game\n" + 
						 "3. Start the game\n" +
						 "4. Display the final results of all games\n" +
						 "5. Display the points of all athletes\n" +
						 "6. Exit\n\n" +
						 "Enter an option:");
	}
	public static void displayGameTypeMenu()
	{
		System.out.print("Game Type Option:\n" +
						 "===================================\n" + 
						 "1. Swimming\n" +
						 "2. Cycling\n" + 
						 "3. Runnung\n" +
						 "4. Back to main menu\n" +
						 "Enter an option:");
	}
	
	public static boolean showUserChoiceWarning(String input)
	{
		if(input.length() >= 32)
		{
			System.out.println("Input overflow, please enter valid range\n");
			return true;
		}
		
		System.out.println("Input Invalid, please make sure can only enter number\n");
		return true;
	}
	public static boolean inputValidation_Main(int input)
	{
		if(input < 0 || input > 6)
		{
			System.out.println("Please enter valid range.\n");
			return false;
		}
		return true;
	}
	// check valid input under the 1.select game type
	public static boolean inputValidation_Sub(int input)
	{
		if(input < 0 || input > 4)
		{
			System.out.println("Please enter valid range.\n");
			return false;
		}
		if(input == 4)
			return false;
		return true;
	}
	// check valid input under the 2.predict the winner
	public static boolean inputValidation_Sub(int input, int listSize)
	{
		if(input < 0 || input > listSize)
		{
			System.out.println("Please enter valid range.\n");
			return false;
		}
		return true;
	}
	// display error message when game object is uninitialized
	public static boolean errorMsg_GameUninitialized()
	{
		System.out.println("Please select game type first.");
		return true;
	}
	// display error message when game object is unexecuted
	public static boolean errorMsg_GameUnexecuted()
	{
		System.out.println("Please start game to get result.");
		return true;
	}
	public static void errorMsg_InvalidateCandidateList()
	{
		System.out.println("Candidate number is invalidate, game is cancelled.\n" +
						   "Please select game type again.");
	}
	public static void gameOver()
	{
		System.out.println("Game Over!!");
	}
	//------------------------------------------------------------------------------
}
