package Tute09;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class Guess extends Application {
	@Override // Override the start method in the Application class
	public void start(Stage primaryStage) {
		double WIDTH = 400;
		double HEIGHT = 300;
		String str;
		Pane pane = new Pane();
		for (int i = 0; i <= 2; i++) {
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(100.0);
			if (i == 0) {
				str = "Tests -- 20%";
				rectangle.setHeight(50.0);
				rectangle.setFill(Color.RED);
			} 
			else if( i == 1){
				str = "Assignments -- 40%";
				rectangle.setHeight(200.0);
				rectangle.setFill(Color.BLUE);
			}
			else
			{
				str = "Exams -- 30%";
				rectangle.setHeight(150.0);
				rectangle.setFill(Color.GREEN);
			}
			rectangle.setX((i * 120) + 10);
			rectangle.setY(HEIGHT - rectangle.getHeight());
			pane.getChildren().add(new Text(rectangle.getX(), 
											rectangle.getY() - 20, str));
			pane.getChildren().add(rectangle);
		}
		Scene scene = new Scene(pane, WIDTH, HEIGHT);
		primaryStage.setTitle("AP Accessment BarChart"); // Set the stage title
		primaryStage.setScene(scene); // Place the scene in the stage
		primaryStage.show(); // Display the stage
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}


