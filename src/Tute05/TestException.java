package Tute05;

public class TestException {
	public static void main(String args[])
	{
		try {
			mymethod1();
			System.out.println("Method 1 Done!");
			mymethod2();
			System.out.println("Method 2 Done!");
		} catch (MyOwnException e) {
			System.out.println("My Exception!");
		} catch (Exception eE) {
			System.out.println("Top-level Exception");
		} finally {
			System.out.println("What now?");
		}
		System.out.println("Did system crash?");
	}
	public static void mymethod1() throws MyOwnException
	{
		//throw new MyOwnException();
	}
	public static void mymethod2() throws SomeotherException
	{
		throw new SomeotherException();
	}
}
