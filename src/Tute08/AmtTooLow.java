package Tute08;

public class AmtTooLow extends Exception{
	
	public AmtTooLow(String errMsg, double amount) {
	      super(errMsg); 
	      System.out.println("Amount " + amount); 
	      System.out.println("Error message is: " + errMsg);
	}
}
