/**
 * 
 */
package Tute08;

import static org.junit.Assert.*;
import Tute08.Account;
import Tute08.AmtTooLow;
import Tute08.AmtTooHigh;
import Tute08.InsuffientBalance;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Loso
 *
 */
public class AccountTest {
	Account client;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		client = new Account("loso", "L0001", 10000.0);
	}

	@Test (expected=AmtTooLow.class)
	public void testTooLowExpection() throws AmtTooLow, AmtTooHigh,InsuffientBalance{
		client.withdraw(5.0);
	}
	
	@Test (expected=AmtTooHigh.class)
	public void testTooHightExpection() throws AmtTooLow, AmtTooHigh,InsuffientBalance{
		client.withdraw(1500.0);
	}

}
