package Tute08;

public class Account {
	public final double WITHDRAE_AMOUNT_MIN = 20.0;
	public final double WITHDRAE_AMOUNT_MAX = 1000.0;
	private String name;
	private String ID;
	private double balance;
	
	public Account( String name, String ID, double amt )
	{
		this.name = name;
		this.ID = ID;
		this.balance = amt;
	}
	public double withdraw(double amt) throws AmtTooLow, AmtTooHigh,InsuffientBalance
	{
		if(amt > WITHDRAE_AMOUNT_MAX)
			throw new AmtTooHigh("Amount is too high. Over the maximun!", amt);
		if(amt < WITHDRAE_AMOUNT_MIN)
			throw new AmtTooLow("Amount is too low. Under the minimun!", amt);
		if(balance < amt)
			throw new InsuffientBalance("Balance is less than amount", balance);
		
		balance -= amt;
		return balance;
	}
}
