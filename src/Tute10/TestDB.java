package Tute10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.hsqldb.Server;

import java.io.FileReader;

public class TestDB {
	ArrayList<Employee> employeeList;
	
	public ArrayList<Employee> getEmployeeList()
	{
		return employeeList;
	}
	public void readFile()
	{
		//read file first		
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try {
            br = new BufferedReader(new FileReader("employeeDB.txt"));
            while ((line = br.readLine()) != null) {
            	// use comma as separator
            	String[] content = line.split(cvsSplitBy);
            	Employee emp = new Employee(content[0], content[1], content[2]);
            	employeeList.add(emp);
            }
		}catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    if (br != null) {
		        try {
		             br.close();
		         } catch (IOException e) {
		             e.printStackTrace();
		         }
		    }
		 }
	}
	public static void main(String[] args) {
		TestDB db = new TestDB();
		db.readFile();
            
		Server hsqlServer = null;
		Connection connection = null;
		ResultSet rs = null;
		
		hsqlServer = new Server();
		hsqlServer.setLogWriter(null);
		hsqlServer.setSilent(true);
		hsqlServer.setDatabaseName(0, "TestDB");
		hsqlServer.setDatabasePath(0, "file:MYDB");
		
		hsqlServer.start();
		
		// making a connection
		try {
			Class.forName("org.hsqldb.jdbcDriver");
			connection = DriverManager.getConnection("jdbc:hsqldb:TestDB", "sa", "123");
			
		
			connection.prepareStatement("drop table employee if exists;").execute();
			connection.prepareStatement("create table employee (ENO integer, Name varchar(20) not null, Salary integer);").execute();
			for(int i=0 ; i<db.getEmployeeList().size() ; i++)
			{
				Employee emp = db.getEmployeeList().get(i);
				connection.prepareStatement("insert into employee (ENO, Name, Salary)"  +
									    	"values (" + emp.getENO() + "," + 
									    	emp.getName() + "," + 
									    	emp.getSalary() + ");").execute();
			}
			// query from the db
			rs = connection.prepareStatement("select ENO, Name, Salary from employee;").executeQuery();
			while (rs.next())
			{
				System.out.println(String.format("ENO: %1d, Name: %1s, Salary : %1d", rs.getInt(1), rs.getString(2), rs.getInt(3)));
			}
			
			connection.commit();
		} catch (SQLException e2) {
			e2.printStackTrace();
		} catch (ClassNotFoundException e2) {
			e2.printStackTrace();
		}
		
		
		
		// end of stub code for in/out stub
	}
	
}