package Tute10;

public class Employee {
	private int ENO;
	private String Name;
	private int Salary;
	
	public Employee()
	{
		
	}
	public Employee(String ENO, String Name, String Salary)
	{
		this.setENO(Integer.parseInt(ENO));
		this.setName(Name);
		this.setSalary(Integer.parseInt(Salary));
	}
	public int getENO() {
		return ENO;
	}
	public void setENO(int eNO) {
		ENO = eNO;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getSalary() {
		return Salary;
	}
	public void setSalary(int salary) {
		Salary = salary;
	}
}
