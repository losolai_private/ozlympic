package Tute06;

public class TestGenericMethods {
	public static void main(String args[]) {
		Sample<Integer> obj = new Sample<Integer>(42);
		obj.<String> show(new String("Hello"));
	}
}
