package Tute06;

class Sample<T> {
	private T data;

	public Sample(T data) {
		this.data = data;
	}

	public <ViewerType> void show(ViewerType viewer) {
		System.out.println("Viewer is: " + viewer);
		System.out.println("Data is: " + data);
	}
}
